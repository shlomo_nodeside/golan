(function() {
  'use strict';

  angular.module('golan')
    .controller('ObCtrl', ctrl)           //  Main Ob Controller
    .controller('obModalCtrl', modalCtrl) //  Large Image Modal
    .directive('deleteOb', deleteOb)      //  Delete ob btn
    .directive('editOb', editOb)          //  Edit Ob btn
  ;

  function ctrl($scope, $stateParams, restApi, $uibModal, userInfo) {

    var itemId = $stateParams.id;

    $scope.userInfo = userInfo;
    $scope.mapOptions = {
      zoom: 13,
      options: {
        scrollwheel: false
      }
    };

    $scope.item = {};
    $scope.similars = [];
    $scope.loading = true;

    $scope.showLargeImage = function (index) {

      if (!angular.isDefined($scope.item.observation_photos[index])) {
        return false;
      }

      var modalInstance = $uibModal.open({
        template: '<i class="fa fa-times-circle close" ng-click="close()"></i><p>{{title}}</p><img ng-src="{{img.photo.large_url}}"><p class="license tar text-muted">רישיון תמונה: {{license}}</p>',
        controller: 'obModalCtrl',
        windowClass: 'img-modal',
        size: 'lg',
        resolve: {
          item: function () {
            return {
              img: $scope.item.observation_photos[index],
              item: $scope.item
            };
          }
        }
      });

      modalInstance.result.then();
    };

    //  Filter Out on set custom fileds
    $scope.filterFileds = function (filed) {
      return filed.value !== 'לא ידוע';
    };

    var getItemSimilars = function () {
      restApi.obs.similar({item: $scope.item})
        .then(function (data) {
          //console.log($scope.item, data);

          $scope.loading = false;
          $scope.similars = data.items;
        });
    };

    var getItems = function () {
      $scope.item = null;
      $scope.loadingItems = true;
      $scope.slideReady = false;
      imgLoadedCounter = 0;

      restApi.obs.single({id: itemId})
        .then(
          function (data) {

            $scope.item = data.items;

            //  Merge Comments & identification
            $scope.item.ccInfo.discussion = $scope.item.identifications.concat($scope.item.comments);

            $scope.item.ccInfo.useLargeIcon = true;

            //  Get Similar
            getItemSimilars();

            //  If we have position, update Map
            if ($scope.item.latitude) {
              $scope.mapOptions.zoom = 1;
              $scope.mapOptions.center = {
                lat: $scope.item.latitude,
                lng: $scope.item.longitude
              };
            }

          },
          function (err) {
            console.log('Error: ', err);
          }
        );
    };


    //  Load Slick Slide Only when all images has been loaded
    var imgLoadedCounter = 0;
    $scope.imgLoad = function () {
      //  Count This Load event.
      imgLoadedCounter++;

      //  all Done?
      if (
        imgLoadedCounter === $scope.item.observation_photos.length * 2
        || $scope.item.observation_photos.length === 1
      ) {

        $('.nav-gallery').slick({
          slidesToShow: 3,
          arrows: false,
          slidesToScroll: 1,
          asNavFor: ".main-gallery",
          centerMode: true,
          focusOnSelect: true
        });

        $('.main-gallery').slick({
          arrows: false,
          adaptiveHeight: true
        });
        $scope.slideReady = true;
      }
    };

    //  Get The Item, Now
    getItems();
    
    //  Event Fires when new desiction object created (Works for identification and comments)
    $scope.$on('addDisction', function (event, data) {

      var userObj = {
        id: userInfo.id,
        login: userInfo.login,
        name: userInfo.name,
        'user_icon_url': userInfo.user_icon_url
      };

      data['user'] = userObj;
      $scope.item.ccInfo.discussion.push(data);
    });

    $scope.$on('CCImagesLoaded', getItems);
    $scope.$on('updateItem', getItems);

  }

  //  Delect Ob Btn
  function deleteOb(restApi, AuthService, $uibModal){

    var controller = function ($scope, $state) {

      var vm = this;

      //  Test if we should render btn
      var shouldShow = function(){

        //  - User Should be logged-in
        //  - Identification should BE owened by current user

        //  Reset
        vm.show = false;

        //  Test
        AuthService.getUserInfo().then(function(user){

          if(user.id === vm.item.user_id){
            vm.show = true;
          }

        });
      };

      vm.show = false;
      vm.loading = false;

      shouldShow();

      //  Send Agree Notice To backend
      vm.submit = function(){
        vm.loading = true;

        //  Modal Verify delete
        var modalInstance = $uibModal.open({
          controller: function($scope,$uibModalInstance){

            $scope.ok = $uibModalInstance.close;
            $scope.cancel = $uibModalInstance.dismiss;

          },
          template: '<div class="modal-header"><h4 class="modal-title">האם באמת ברצונך למחוק תצפית זו? </h4></div>' +
            '<div class="modal-body">כל המידע הקשור אליה יימחק גם כן, ללא אפשרות לשחזור.</div>' +
          '<div class="modal-footer"><button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button><button class="btn btn-danger" type="button" ng-click="ok()">מחק</button></div>',

        });

        //  User Aproved!
        modalInstance.result.then(function () {

          //  Send Taxa to backend
          restApi.cake.deleteOb(vm.item.id).then(function () {
            vm.loading = false;
            //  This Obs does not exsits any more, Go home
            $state.go('home');
          });
        });
      };

      $scope.$on('loginChanged', shouldShow);
    };

    return {
      restrict:         "E",
      scope :           true,
      bindToController: {item: '='},
      controller:       controller,
      controllerAs:     'deleteOb',
      template:         '<button class="btn btn-sm btn-danger" ng-click="deleteOb.submit()" ng-if="deleteOb.show">מחיקת תצפית</button>'
    };
  }

  //  Edit Ob Btn
  function editOb(AuthService){

    var controller = function ($scope) {

      var vm = this;

      //  Test if we should render btn
      var shouldShow = function(){

        //  - User Should be logged-in
        //  - Identification should BE owened by current user

        //  Reset
        vm.show = false;

        //  Test
        AuthService.getUserInfo().then(function(user){

          if(user.id === vm.item.user_id){
            vm.show = true;
          }

        });
      };

      vm.show = false;

      shouldShow();

      $scope.$on('loginChanged', shouldShow);
    };

    return {
      restrict:         "E",
      scope :           true,
      bindToController: {item: '='},
      controller:       controller,
      controllerAs:     'editOb',
      template:         '<button class="btn btn-sm btn-primary" ui-sref="obs.edit({id: editOb.item.id})" ng-if="editOb.show">עריכה</button>'
    };
  }

  function modalCtrl($scope, $uibModalInstance, item) {

      $scope.img = item.img;
      $scope.title = item.item.ccInfo.name;
      $scope.license = item.img.photo.license_code;

      $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }

})();