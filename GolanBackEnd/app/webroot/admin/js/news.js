/**
 * News Admin Controller
 */
'use strict';
angular.module('golanAdmin')
.controller('newsCtrl', function($scope, $http, $uibModal, Notification, $window){

  var backEndData = $window.app.golanBackEnd;

  var getItems = function(){
      $http.get(backEndData.urls.list)
        .then(function(data){

          $scope.items = data.data.items;
      })
    };

  $scope.items = [];

  $scope.del = function(item){
    var modalInstance = $uibModal.open({
      templateUrl: 'confirmDelModal.html',
      controller: 'confirmDelModalCtrl',
      size: 'sm',
      resolve: {
        item: function () {
          return item;
        }
      }
    });

    modalInstance.result.then(
      function () {

        //  Delete Approved, do it man
        $scope.loading = true;

        $http.get(backEndData.urls.del + '/' + item.id).then(function(){
          $scope.loading = true;
          Notification.success('הפריט נמחק בהצלחה');
          getItems();
        });
      }
    );
  };

  $scope.edit = function(item){
    $scope.activeItem = item;
  };

  getItems();

  $scope.$on('update', function () {
    getItems();
  });

})

.directive('ccAddEdit', function($window, $sce, Notification, $http){

  var addUrl = $window.app.golanBackEnd.urls.add;

  var link = function($scope, $element, attrs){

    //  Context is the switch between ADD & EDIT
    $scope.context = 'add';

    $scope.save = function(){
      var xsrf = $.param($scope.item);
      $http({
        method: 'POST',
        url: addUrl,
        data: xsrf,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(
        function(){
          Notification.success('חדשה נשמרה בהצלחה');
          $scope.item = null;
          $scope.$emit('update');
        },
        function(){
          Notification.error('השמירה נכשלה');
        }
      );


    };

    $scope.$watch('item', function(value) {

      if(value){
        $scope.context = 'edit';
      }else {
        $scope.context = 'add';
      }
      //console.log('Item Change', $scope.context,value);
    });

    //  Reset to add Mode
    $scope.add = function(){
      $scope.item = null;
      $scope.context = 'add';
    };
  };

  return {
    restrict: 'E',
    templateUrl: $sce.trustAsResourceUrl(addUrl),
    scope: {
      item: '='
    },
    link: link
  };
})
;