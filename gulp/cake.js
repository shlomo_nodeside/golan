'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del', 'run-sequence']
});

var conf = require('./conf');
var cakeWebroot = './GolanBackEnd/app/webroot/angular';


gulp.task('cakeClean', function(){
  return $.del([cakeWebroot]);
});

gulp.task('cake', function(){
  //  Copy Scripts and Styles
  gulp.src([conf.paths.dist + '/**/*.js', conf.paths.dist + '/**/*.css'])
    .pipe($.rename(function (path) {
      path.basename = path.basename.split("-").shift();
    }))
    .pipe(gulp.dest(cakeWebroot + '/'));

  gulp.src(conf.paths.dist + '/fonts/**')
    .pipe(gulp.dest(cakeWebroot + '/fonts'));


  gulp.src([conf.paths.dist + '/assets/images/**'])
    .pipe(gulp.dest( cakeWebroot + '/images'));

  //  Copy Style Images
  gulp.src([conf.paths.dist + '/assets/**', '!' + conf.paths.dist + '/assets/images/**'])
    .pipe(gulp.dest( cakeWebroot + '/assets'));


  return;
});