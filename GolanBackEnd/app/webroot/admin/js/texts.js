/**
 * News Admin Controller
 */
'use strict';
angular.module('golanAdmin')
    .controller('TextsCtrl', function($scope, $http, Notification, $window, $sce){

        var backEndData = $window.app.golanBackEnd;

        $scope.translation = {
            about : 'אודות',
            activity : 'פעילות',
            contact : 'צור קשר',
            links : 'קישורים',
            join : 'הצטרפות',
            'about_bottom' : 'אודות (למטה)'
        };

        $scope.items = backEndData.list;

        $scope.trust = function(content) {
            return $sce.trustAsHtml(content);
        };

        $scope.save = function(item){

            var xsrf = $.param(item.texts);
            
            $http({
                method: 'POST',
                url: backEndData.urls.edit,
                data: xsrf,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(
              function(){
                  Notification.success('טקסט נשמר בהצלחה');
                  item.edit = false;
              },
              function(){
                  Notification.error('השמירה נכשלה');
              }
            );
        };


    })

;
