<?php

$intArr = array(
    'urls'  => array(
        'list'  => $this->Html->Url('/json/articles', true),
        'del'   => $this->Html->Url('del', true),
        'add'   => $this->Html->Url('add', true)
    ),
    'cats'  => $cats
);

$this->Js->set('golanBackEnd', $intArr);
echo $this->Js->writeBuffer(array('onDomReady' => false));

?>
<div class="curator" ng-controller="ArticlesCtrl">
    <div class="page-header">
        <h3>רשימת מאמרים</h3>
    </div>

    <div class="row">

        <div class="col-md-6">

            <div class="panel panel-primary cc-list">
                <div class="panel-heading">מאמרים שמורים</div>

                <div class="panel-body">

                    <div class="list-group">
                        <div class="list-group-item" ng-repeat="item in items" ng-class="{active: item.id === activeItem.id}">
                            <h4 class="list-group-item-heading">{{item.subject}}</h4>

                            <div class="pull-left actions">
                                <i class="fa fa-times pointer"  title="" ng-click="del(item)"></i>
                                <i class="fa fa-pencil pointer" title="" ng-click="edit(item)"></i>
                            </div>
                            <p class="list-group-item-text">
                                {{item.author}}
                                <button ng-if="item.cat_id" class="btn btn-default btn-xs" disabled>{{item.category.name}}</button>
                            </p>
                            <small>{{item.published | simpleMysql | date:'dd-MM-yyyy'}}</small>
                        </div>
                    </div>

                    <div class="alert alert-warning" ng-show="!items.length">לא מוגדרים מאמרים</div>
                </div>
            </div>

        </div><!-- col-6 -->

        <div class="col-md-6">
            <cc-add-edit-articles item="activeItem"></cc-add-edit-articles>
        </div>

    </div><!-- row -->
</div>


<script type="text/ng-template" id="confirmDelModal.html">
    <div class="modal-header">
        <h5 class="modal-title">
            <span>האם למחוק את</span> {{item.subject}}
        </h5>
    </div>
    <div class="modal-footer tal">
        <button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button>
        <button class="btn btn-primary" type="button" ng-click="ok()">מחק</button>
    </div>
</script>