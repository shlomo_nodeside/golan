(function() {
  'use strict';

  angular.module('golan')
    .run(cacheChange)
    .controller('AddObController', controller)
    .directive('fileModel', function ($parse) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
            scope.$apply(function(){
              modelSetter(scope, element[0].files[0]);
            });
          });
        }
      };
    })
  ;

  function cacheChange($templateCache) {
    $templateCache.put("template/timepicker/timepicker.html",
      "<table>\n" +
      "  <tbody>\n" +
      "    <tr class=\"text-center\" ng-show=\"::showSpinners\">\n" +
      "      <td><a ng-click=\"incrementMinutes()\" ng-class=\"{disabled: noIncrementMinutes()}\" class=\"btn btn-link\" ng-disabled=\"noIncrementMinutes()\" tabindex=\"{{::tabindex}}\"><span class=\"fa fa-chevron-up\"></span></a></td>\n" +
      "      <td>&nbsp;</td>\n" +
      "      <td><a ng-click=\"incrementHours()\" ng-class=\"{disabled: noIncrementHours()}\" class=\"btn btn-link\" ng-disabled=\"noIncrementHours()\" tabindex=\"{{::tabindex}}\"><span class=\"fa fa-chevron-up\"></span></a></td>\n" +
      "      <td ng-show=\"showMeridian\"></td>\n" +
      "    </tr>\n" +
      "    <tr>\n" +
      "      <td class=\"form-group\" ng-class=\"{'has-error': invalidMinutes}\">\n" +
      "        <input style=\"width:50px;\" type=\"text\" ng-model=\"minutes\" ng-change=\"updateMinutes()\" class=\"form-control text-center\" ng-readonly=\"::readonlyInput\" maxlength=\"2\" tabindex=\"{{::tabindex}}\">\n" +
      "      </td>\n" +
      "      <td>:</td>\n" +
      "      <td class=\"form-group\" ng-class=\"{'has-error': invalidHours}\">\n" +
      "        <input style=\"width:50px;\" type=\"text\" ng-model=\"hours\" ng-change=\"updateHours()\" class=\"form-control text-center\" ng-readonly=\"::readonlyInput\" maxlength=\"2\" tabindex=\"{{::tabindex}}\" ng-focus=\"add.setFocus(2)\">\n" +
      "      </td>\n" +
      "      <td ng-show=\"showMeridian\"><button type=\"button\" ng-class=\"{disabled: noToggleMeridian()}\" class=\"btn btn-default text-center\" ng-click=\"toggleMeridian()\" ng-disabled=\"noToggleMeridian()\" tabindex=\"{{::tabindex}}\">{{meridian}}</button></td>\n" +
      "    </tr>\n" +
      "    <tr class=\"text-center\" ng-show=\"::showSpinners\">\n" +
      "      <td><a ng-click=\"decrementMinutes()\" ng-class=\"{disabled: noDecrementMinutes()}\" class=\"btn btn-link\" ng-disabled=\"noDecrementMinutes()\" tabindex=\"{{::tabindex}}\"><span class=\"fa fa-chevron-down\"></span></a></td>\n" +
      "      <td>&nbsp;</td>\n" +
      "      <td><a ng-click=\"decrementHours()\" ng-class=\"{disabled: noDecrementHours()}\" class=\"btn btn-link\" ng-disabled=\"noDecrementHours()\" tabindex=\"{{::tabindex}}\"><span class=\"fa fa-chevron-down\"></span></a></td>\n" +
      "      <td ng-show=\"showMeridian\"></td>\n" +
      "    </tr>\n" +
      "  </tbody>\n" +
      "</table>\n" +
      "");
  }

  function itemToVm(item) {

     // console.log(item);
    
    //  Fix Project object
    function fixProjects(){
      var fixed = [];

      angular.forEach(item.project_observations, function(project){
        this.push(project.project_id)
      }, fixed);

      return fixed;
    }

    //  Fix Taxon name
    function fixGuess(){
      if(item.taxon){
        return item.taxon.default_name.name;
      }
    }

    return {
      id            : item.id,
      date          : item.observed_on,
      description   : item.description,
      latitude      : item.latitude ? parseFloat(item.latitude) : null,
      longitude     : item.longitude ? parseFloat(item.longitude) : null,
      geoprivacy    : item.geoprivacy ? item.geoprivacy : 'open',
      projects      : fixProjects(),
      taxon_id      : item.taxon_id,
      species_guess : fixGuess()
    };
  }

  function controller($scope, item,restApi, AuthService, Globals, $filter, $state, $rootScope) {

    //console.log('Initing add Ob Controller', item);

    var vm = this;
    var globals = Globals.get();
    var projectStaticVar = [];  //  For edit case, save obs project status
    var cashedProjects = globals.projects;

    vm.test = function () {
      alert("asd");
    };

    var checkIfProjectCashed = function (projectId) {
          var temp = null;
          angular.forEach(cashedProjects, function (proj) {
            if (parseInt(proj.id) === projectId) {
              temp = proj;
            }
          });

          return temp;
        };

    var getUserProjects = function () {
      AuthService.userAllProjects()
        .then(
          function (data) {
            angular.forEach(data, function (proj) {
              var temp = checkIfProjectCashed(proj.project_id);
              if (temp) {
                proj.smart_flag = parseInt(temp.smart_flag);
                if (proj.smart_flag > 0) {
                  vm.ob.projects.push(proj.project_id);
                }
              }
              this.push(proj);
            }, vm.userProjects);

          });
    };
  
    $rootScope.$on('mapItemDraged', function (event, data) {

        $scope.$apply(function () {

          vm.setFocus(3);
          vm.ob.longitude = data.longitude;
          vm.ob.latitude = data.latitude;
        });
    });

    vm.loadedImages = [];
    vm.userProjects = [];
    getUserProjects();

    vm.projOrder = function (project) {
      return globals.defaultProjects.indexOf(parseInt(project.project.id)) * -1;
    };

    vm.uploader = {};

    vm.activePanel = 1;

    vm.datePicker = {
      options: {},
      isOpen: false,
      maxDate: new Date()
    };

    //  If its edit mode, we need to fix item (inat Obj) structure to fit add mode
    if(item){
      vm.ob = itemToVm(item);
      // console.log(vm.ob);
      projectStaticVar = vm.ob.projects.slice();

      if(item.observation_photos){
        vm.loadedImages = item.observation_photos;
      }
    }else {
      vm.ob = {
        date: new Date().setMinutes(0),
        latitude: 33.010636,
        longitude: 35.758098,
        projects: [],
        description: null,
        taxon_id: null,
        species_guess: null,
        geoprivacy: 'open'
      };
    }

    vm.mapOptions = {
      zoom: 10,
      center : {
        lat: vm.ob.latitude,
        lng: vm.ob.longitude
      },
      options: {
        scrollwheel: false
      }
    };

    vm.setFocus = function (panelIndex) {
      if (panelIndex === 99) {
        vm.activePanel++;
      } else {
        vm.activePanel = panelIndex;
      }
    };

    vm.toggleProject = function (project) {
      vm.setFocus(5);

      //  Smart project assosc
      if (project.smart_flag && project.smart_flag === 2) {
        return false;
      }

      if (vm.ob.projects.indexOf(project.project.id) === -1) {
        vm.ob.projects.push(project.project.id);
      } else {
        vm.ob.projects.splice(vm.ob.projects.indexOf(project.project.id), 1);
      }
    };

    vm.isActive = function (project) {
      return $scope.ob.projects.indexOf(project.id) !== -1;
    };

    vm.updateMarker = function () {
      vm.map.marker.coords = {
        latitude: vm.ob.latitude,
        longitude: vm.ob.longitude
      }
    };
       
    vm.send = function () {

      var useFunction = 'addOb';
      //  If is Editing Mode
      if(vm.ob.id){
        
        useFunction = 'editOb';

        //  Edit mode need some changes on projects

        //  Find what (if) project are ADDED
        vm.ob.projectsToAdd = [];
        angular.forEach(vm.ob.projects, function (project) {
          if(projectStaticVar.indexOf(project) === -1){
            this.push(project);
          }
        }, vm.ob.projectsToAdd);

        //  Find what (if) project are REMOVED
        vm.ob.projectsToRemove = [];
        angular.forEach(projectStaticVar, function (project) {
          if(vm.ob.projects.indexOf(project) === -1){
            this.push(project);
          }
        }, vm.ob.projectsToRemove);

      }

      //  If user has set a real taxon id, clear the free text
      if(vm.ob.taxon_id){
        vm.ob.species_guess = null;
      }

      //  Fix Date Format
      vm.ob.observed_on_string = $filter('date')(vm.ob.date, 'yyyy-MM-dd HH:mm');

      restApi.inat[useFunction](vm.ob ,vm.uploader.flow.files).then(
        function (data) {
          // console.log('OK');
          //  OKE, go to the new ob page
          $state.go('obs.single', {id: data.id});
        },
        function () {
          //  ToDO: Error notifications
        }
      );
    };

  }

})();