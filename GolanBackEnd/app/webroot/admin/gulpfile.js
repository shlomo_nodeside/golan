var gulp            = require('gulp'),
    csso            = require('gulp-csso'),
    sass            = require('gulp-sass'),
    concat          = require('gulp-concat'),
    mainBowerFiles  = require('main-bower-files'),
    gulpFilter      = require('gulp-filter'),
    uglify          = require('gulp-uglify'),
    replace         = require('gulp-replace'),
    gulpIgnore      = require('gulp-ignore'),
    sourcemaps      = require('gulp-sourcemaps'),
    ngAnnotate      = require('gulp-ng-annotate'),
    browserSync     = require('browser-sync').create(),
    order           = require("gulp-order"),
    del             = require('del');
var distLib = './dist/';

gulp.task('default', ['watch']);

gulp.task('bower', function () {

    var jsFilter = gulpFilter('**/*.js', {restore: true}),
        cssFilter = gulpFilter('**/*.css', {restore: true}),
        fontsCondition = [
            '**/*.less',
            '**/*.eot',
            '**/*.otf',
            '**/*.svg',
            '**/*.ttf',
            '**/*.woff',
            '**/*.woff2'
        ];

    console.log('Getting vendor scripts and styles');
    gulp.src(mainBowerFiles())
        .pipe(jsFilter)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        .pipe(concat('vendor.css'))
        .pipe(replace('../fonts/', './fonts/'))
        .pipe(csso())
        .pipe(cssFilter.restore)
        .pipe(gulpIgnore.exclude(fontsCondition))
        .pipe(gulp.dest(distLib));

    //  Copy font to directory
    console.log('Getting Vendor fonts');
    gulp.src(mainBowerFiles())
        .pipe(gulpFilter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe(gulp.dest(distLib +'fonts/'));


});

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(csso())
            .pipe(concat('admin.css'))
        .pipe(sourcemaps.write('./maps', {
            sourceRoot: 'http://localhost/golan/GolanBackEnd/admin/sass/'
        }))
        .pipe(gulp.dest(distLib))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function(){
  return gulp.src('./js/**/*.js')
    .pipe(order([
      'js/app/_app.js',
      'js/**/*.js'
    ]))
    .pipe(sourcemaps.init())
    .pipe(concat('admin.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(sourcemaps.write('./maps', {
        sourceMappingURL: '/maps/admin.js.map',
        sourceRoot: 'http://localhost/golan/GolanBackEnd/admin/js/'
    }))
    .pipe(gulp.dest(distLib));
});

// Rerun the task when a file changes
gulp.task('watch', function() {

    browserSync.init({
        proxy: 'http://localhost/golan/GolanBackEnd/control/'
    });

    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch("js/**/*.js", ['js-watch']);
    //gulp.watch('http://localhost/golan/GolanBackEnd/admin/dist/admin.js', ['js-watch']);

});

//  Do It all!
gulp.task('build', ['bower','sass','scripts']);

gulp.task('js-watch', ['scripts'],function(){
    console.log('Finish scripting');
    browserSync.reload();
});