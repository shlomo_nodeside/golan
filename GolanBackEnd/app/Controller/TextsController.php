
<?php
App::uses('AppController', 'Controller');

class TextsController extends AppController {

	/*
	 * Control Texts in app Pages
	 * NOT supporting adding AND deleting
	 */

	public function control_index()
	{
		$this->headerMenu['text'] = 'active';
		$this->set('title_for_layout', 'טקסטים');

		$texts = $this->Text->query("SELECT * FROM texts");
		$this->set('texts', $texts);
	}

	public function control_edit()
	{
		$this->autoRender = false;

		if ($this->request->is('post') && isset($this->request->data['id']))
		{
			$this->Text->id = $this->request->data['id'];

			if($this->Text->save($this->request->data)){
				return json_encode(array('success'=> true));
			}else {
				return json_encode(array('success'=> false));
			}
		}

		return json_encode(array('success'=> false));
	}

	// JSON API functions

	//	Get all text, and arrange array by slug
	public function json_index(){

		$texts = $this->Text->Find('all');
		$output = array();
		foreach($texts as $text){
			$output[$text['Text']['slug']] = $text['Text']['content'];
		}


		return json_encode($output);

	}

	//	Get Text by Slug - Will Not be used for now
	public function json_get($slug = null){

		$this->autoRender = false;

		if(empty($slug))
			return false;

		$text = $this->Text->findBySlug($slug);

		if(isset($text['Text']))
			return $text['Text']['content'];

		return false;

	}

}