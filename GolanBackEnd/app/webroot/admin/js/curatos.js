/**
 * News Admin Controller
 */
'use strict';
angular.module('golanAdmin')
  .controller('CuratorsCtrl', function($scope, $http, $uibModal, Notification, $window){

    var backEndData = $window.app.golanBackEnd;

    var getItems = function(){
      $http.get(backEndData.urls.list).then(function(data){
        $scope.items = data.data;
      })
    };

    $scope.items = [];

    $scope.del = function(item){
      var modalInstance = $uibModal.open({
        templateUrl: 'confirmDelModal.html',
        controller: 'confirmDelModalCtrl',
        size: 'sm',
        resolve: {
          item: function () {
            return item;
          }
        }
      });

      modalInstance.result.then(
        function () {

          //  Delete Approved, do it man
          $scope.loading = true;

          $http.get(backEndData.urls.del + '/' + item.id).then(function(){
            $scope.loading = true;
            Notification.success('הפריט נמחק בהצלחה');
            getItems();
          });
        }
      );
    };

    getItems();

    $scope.$on('update', function () {
      getItems();
    });

  })

  .directive('ccAddEditCurator', function($window, $sce, Notification, $http){

    var addUrl = $window.app.golanBackEnd.urls.add;

    var link = function($scope){

      $scope.save = function(){
        var xsrf = $.param($scope.item);
        $http({
          method: 'POST',
          url: addUrl,
          data: xsrf,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
          function(){
            Notification.success('אוצר נוסף בהצלחה');
            $scope.item = null;
            $scope.$emit('update');
          },
          function(){
            Notification.error('השמירה נכשלה');
          }
        );


      };

    };

    return {
      restrict: 'E',
      templateUrl: $sce.trustAsResourceUrl(addUrl),
      scope: {
        item: '='
      },
      link: link
    };
  })
;