/**
 * News Admin Controller
 */
'use strict';
angular.module('golanAdmin')
    .controller('ArticlesCtrl', function($scope, $http, $uibModal, Notification, $window){

        var backEndData = $window.app.golanBackEnd;

        var getItems = function(){
            $http.get(backEndData.urls.list).then(function(data){
                $scope.items = data.data;
            })
        };

        $scope.items = [];

        $scope.del = function(item){
            var modalInstance = $uibModal.open({
                templateUrl: 'confirmDelModal.html',
                controller: 'confirmDelModalCtrl',
                size: 'sm',
                resolve: {
                    item: function () {
                        return item;
                    }
                }
            });

            modalInstance.result.then(
                function () {

                    //  Delete Approved, do it man
                    $scope.loading = true;

                    $http.get(backEndData.urls.del + '/' + item.id).then(function(){
                        $scope.loading = true;
                        Notification.success('מאמר נמחק בהצלחה');
                        getItems();
                    });
                }
            );
        };

        $scope.edit = function(item){
            $scope.$broadcast('editItem', item);
        };

        getItems();

        $scope.$on('update', function () {
            getItems();
        });

    })

    .directive('ccAddEditArticles', function($window, $sce, Notification, $http, $filter){

        var addUrl = $window.app.golanBackEnd.urls.add,
            cats   = $window.app.golanBackEnd.cats;

        var link = function($scope){

            $scope.item = {
                //published: new Date()
            };

            $scope.dumbDate = new Date();

            $scope.dateModel = new Date();

            $scope.dateOptions = {
                startingDay: 1
            };

            $scope.datePop = {
                opened : false
            };

            //  Context is the switch between ADD & EDIT
            $scope.context = 'add';

            $scope.cats = cats;

            $scope.save = function(){

                if(!$scope.item){
                    return false;
                }

                //  Manualy make sure all fileds are checked
                angular.forEach($scope.addArticle.$error.required, function(field) {
                    field.$setDirty();
                });
                
                if($scope.addArticle.$valid && !$scope.addArticle.$untouched){

                    var fd = new FormData();

                    fd.append('file',$scope.item.file);

                    angular.forEach($scope.item, function(value, key){

                        // Fix Date to mysql format
                        if(key === 'published'){
                            value = $filter('date')(value, 'yyyy-MM-dd 00:00:00');
                        }

                        if(key !== 'file'){
                            this.append('data[Article]['+ key +']', value);
                        }
                    }, fd);

                    //  Post THIS
                    $http.post(addUrl, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })
                        .success(function(data){
                            if(data.success){
                                Notification.success('מאמר נשמר בהצלחה');
                                $scope.item = null;
                                $scope.addArticle.$setPristine();
                                $scope.$emit('update');
                            }else {
                                Notification.error('השמירה נכשלה');
                            }

                        })
                        .error(function(error){
                            Notification.error('השמירה נכשלה');
                        });
                }
            };

            //  Reset to add Mode
            $scope.add = function(){
                $scope.item = null;
                $scope.context = 'add';
            };

            $scope.$on('editItem', function(event, item){
                $scope.addArticle.$setUntouched();

                if(item){

                    item.published = new Date(item.published);
                    $scope.item = item;
                    $scope.context = 'edit';
                }else {
                    $scope.context = 'add';
                }
            });
        };

        return {
            restrict: 'E',
            templateUrl: $sce.trustAsResourceUrl(addUrl),
            scope: {
                item: '='
            },
            link: link
        };
    })

    //  Handle File input Directive
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])

;
