(function() {
  'use strict';
  
  
  angular.module('golan')
    .directive("comments", commentDirective)
    .directive("commentAgree", commentAgreeDirective)
    .directive("removeIdentification", removeIdentification)
  ;

  function commentDirective() {
    return {
      restrict:         "E",
      scope :           true,
      bindToController: { comments: '='},
      controller:       function($scope){},
      controllerAs:     'comments',
      templateUrl:      'app/pages/obs/single/comments/comment.html'
    };
  }

  function commentAgreeDirective(restApi, AuthService){

    var controller = function ($scope) {

      var vm = this;

      //  Test if we should render btn
      var shouldShow = function(){

        //  - User Should be logged-in
        //  - Identification should not be owened by current user

        //  Reset
        vm.show = false;

        //  Test
        AuthService.getUserInfo().then(function(user){

          if(user.id !== vm.comment.user_id){
            vm.show = true;
          }

        });
      };

      vm.show = false;
      vm.loading = false;

      shouldShow();
      
      //  Send Agree Notice To backend
      vm.send = function(){
        vm.loading = true;
        var identification = {
          parent : vm.comment.observation_id,
          specie : vm.comment.taxon_id
        };

        //  Send Taxa to backend
        restApi.inat.identification(identification).then(
          function (data) {
            vm.loading = false;
            $scope.$emit('updateItem');
          },function(error){
            alert('שגיאה בשמירת הזיהוי');
            console.log(error);
            vm.loading = false;
          });
      };

      $scope.$on('loginChanged', shouldShow);
    };

    return {
      restrict:         "E",
      scope :           true,
      bindToController: {comment: '='},
      controller:       controller,
      controllerAs:     'agree',
      template:         '<button class="btn btn-xs btn-success" ng-click="agree.send()" ng-if="agree.show">מסכים לזיהוי<span ng-show="agree.loading" class="padding-right-5"><i class="fa fa-spinner fa-spin "></i></span></button>'
    };
  }

  function removeIdentification(restApi, AuthService){

    var controller = function ($scope) {

      var vm = this;

      //  Test if we should render btn
      var shouldShow = function(){

        //  - User Should be logged-in
        //  - Identification should BE owened by current user

        //  Reset
        vm.show = false;

        //  Test
        AuthService.getUserInfo().then(function(user){

          if(user.id === vm.comment.user_id){
            vm.show = true;
          }

        });
      };

      vm.show = false;
      vm.loading = false;

      shouldShow();

      //  Send Agree Notice To backend
      vm.submit = function(){
        vm.loading = true;
 
        //  Send Taxa to backend
        restApi.cake.deleteIdentification(vm.comment.id).then(function () {
          vm.loading = false;
          $scope.$emit('updateItem');
        });
      };

      $scope.$on('loginChanged', shouldShow);
    };

    return {
      restrict:         "E",
      scope :           true,
      bindToController: {comment: '='},
      controller:       controller,
      controllerAs:     'remove',
      template:         '<button class="btn btn-xs btn-danger" ng-click="remove.submit()" ng-if="remove.show">הסר זיהוי<span ng-show="remove.loading"><i class="fa fa-spinner fa-spin"></i></span></button>'
    };
  }
  
})();