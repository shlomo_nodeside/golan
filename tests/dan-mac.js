// conf.js
// var common = require('./common.js');

exports.config = {
  framework: 'jasmine',

  // Directly or Selenium?
  directConnect: true,
  //seleniumAddress: 'http://localhost:4444/wd/hub',

  // which server?
  baseUrl: 'http://localhost/Golan/GolanBackEnd/',
  // parameters:
  params: {
    login1: { user: 'amitai', pass: 'local2014'},
    window: { w: 1280, h: 850}, // window dimentions, comment out if you don't want that
    sleep: 200,   // default browser.sleep(time)
  },

  // What to test
  specs: ['home/main.js'],

  // Test Suites:
  suites: {
  	current: 'home/main.js',	// special one, the default we're working on right now
  	// users: 'users/*.js',
    // customers: 'customers/*.js',
  },
  // suite: null, 	// all of them
  suite: 'current',	// the one we're working on right now

  // onPrepare
  // onPrepare: 'common.js',
  onPrepare: function()
  {
    // browser.ignoreSynchronization = true; // non ng app

    // common.test();

    // resize window
    if( typeof browser.params.window !== 'undefined' )
    {
      // resize window if so defined
      browser.manage().window().setSize(
        browser.params.window.w, 
        browser.params.window.h); //.maximize();  
    }

    // login
    // common.login(browser.params.login1.user,
    //   browser.params.login1.pass );
  },
  
  // capabilities: {
  //   browserName: 'safari'
  // }
  // multiCapabilities: [	
  	// { browserName: 'chrome' },
  	// { browserName: 'safari' },	// can't work with directConnect
  // ]
}
