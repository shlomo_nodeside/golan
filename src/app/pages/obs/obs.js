(function() {
  'use strict';

  angular.module('golan')
    .config(obsRoute)
  ;

  var obsUrl = 'app/pages/obs/';

  function obsRoute($stateProvider) {
    $stateProvider
      .state('obs.single', {
        url: 'view/:id',
        templateUrl: obsUrl + 'single/single.html',
        controller: 'ObCtrl'
      })
      .state('obs.add', {
        url: 'add',
        templateUrl: obsUrl + 'add/addob.html',
        controller: 'AddObController',
        controllerAs: 'add',
        resolve : {
          item: function (userInfo, AuthService, $q) {
            //  Check that user is logged in
            if(!userInfo){

              //  No User info, check if user is logged in
              AuthService.onLoadStatus().then(
                function () {
                  deferred.resolve(null);
                },
                function () {
                  deferred.reject({
                    type: 1,
                    redirect: 'obs.add',
                    notify : 'יש לתהחבר למערכת על מנת ליצור תצפית חדשה'
                  });
                }
              );
              var deferred = $q.defer();
              return deferred.promise;

            }
            //  Has user info return null
            return null;
          }}
      })
      .state('obs.edit', {
        url: 'edit/:id',
        templateUrl: obsUrl + 'add/addob.html',
        controller: 'AddObController',
        controllerAs: 'add',
        resolve: {
          
          //  Get Item data before loading edit page
          item: function ($stateParams, restApi, $q, userInfo) {

            console.log();

            var deferred = $q.defer();

            //  Verify we have an item id
            if(!$stateParams.id){
              deferred.reject({
                notify : 'שגיאה בעריכת הפריט',
                type: 99
              });

            }else {
              var item;
              //  Get Item Data
              restApi.obs.single({id: $stateParams.id}).then(function(data){
                //  Check user really own this ob
                if(userInfo.id === data.items.user_id){

                  //  Ok, send item to view
                  deferred.resolve(data.items);
                }else {
                  //  Err, this user cannot edit this item
                  deferred.reject({
                    notify : 'הפריט לא שייך למשתמש',
                    type: 99
                  });
                }
              });
            }



            return deferred.promise;
          }
        }

      });
  }

})();