-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.42 - Source distribution
-- Server OS:                    osx10.6
-- HeidiSQL Version:             9.2.0.4948
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for GolanLayer
CREATE DATABASE IF NOT EXISTS `GolanLayer` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `GolanLayer`;


-- Dumping structure for table GolanLayer.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `author_id` varchar(50) NOT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `published` timestamp NULL DEFAULT NULL,
  `published_at` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.articles: ~2 rows (approximately)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `subject`, `content`, `author_id`, `cat_id`, `created_at`, `published`, `published_at`) VALUES
	(1, 'מאמר חשוב ביותר', 'הפרופסור ככה וככה אמר שלא יכול להיות', '', 1, '2016-01-03 12:20:44', NULL, NULL),
	(2, 'מאמר חשוב ביותר', 'הפרופסור ככה וככה אמר שלא יכול להיות', '', 1, '2016-01-03 12:22:44', NULL, NULL);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`) VALUES
	(1, 'כללי'),
	(2, 'מאמרים אקדמיים');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.curators
CREATE TABLE IF NOT EXISTS `curators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.curators: ~1 rows (approximately)
/*!40000 ALTER TABLE `curators` DISABLE KEYS */;
INSERT INTO `curators` (`id`, `login`) VALUES
	(1, '116494');
/*!40000 ALTER TABLE `curators` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.links
CREATE TABLE IF NOT EXISTS `links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.links: ~1 rows (approximately)
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` (`id`, `url`, `created_at`, `title`) VALUES
	(2, 'http://www.ynet.co.il', '2015-12-15 15:16:25', 'ואי ואי ואי נט');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) NOT NULL,
  `short` text,
  `content` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.news: ~2 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `headline`, `short`, `content`, `updated_at`) VALUES
	(1, 'מה קורה גבר?', '<p>תקצירוש שלנווזה ככה<br/>מה קורה?</p>', '<p>אתה, כן, אתה - מה קורה? <br/>זו חדשה חדשה לגמריולכן משהו באמת קורה פה</p>', '2015-12-15 14:54:04'),
	(2, 'מכבי חיפה ניצחה', '<p>בלה בלה <b>בלהיש</b> תקציר<br/>יש לנו כבשה <a href="http://backinblack.com" target="">שחורה</a></p>', '<p>קבוצה של ילדים קטנים מכיתה ג\' טיפולית,בפנדלים</p><p>חחחחחח :-)</p>', '2015-12-15 15:27:04');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.pics
CREATE TABLE IF NOT EXISTS `pics` (
  `id` int(10) unsigned NOT NULL,
  `name` tinytext NOT NULL,
  `url` tinytext NOT NULL,
  `order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.pics: ~7 rows (approximately)
/*!40000 ALTER TABLE `pics` DISABLE KEYS */;
INSERT INTO `pics` (`id`, `name`, `url`, `order`) VALUES
	(1884379, 'Romulea', 'http://static.inaturalist.org/photos/2296958/medium.JPG?1440313511', 5),
	(2100034, 'Common Tortoise', 'http://static.inaturalist.org/photos/2514886/medium.?1444647976', 4),
	(2360581, 'Sus scrofa', 'http://static.inaturalist.org/photos/2626259/medium.jpg?1446998865', 3),
	(2364190, 'נשר שותק', 'http://static.inaturalist.org/photos/2630791/medium.jpg?1447060999', 6),
	(2382481, 'חתול תעלול', 'http://static.inaturalist.org/photos/2655506/medium.jpg?1447572260', 0),
	(2430568, 'עירית גדולה', 'http://static.inaturalist.org/photos/2707399/medium.?1448641986', 2),
	(2441790, 'חזיר בר', 'http://static.inaturalist.org/photos/2724277/medium.jpg?1448962896', 1);
/*!40000 ALTER TABLE `pics` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.projects
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL,
  `title` tinytext NOT NULL,
  `slug` tinytext NOT NULL,
  `icon_url` tinytext NOT NULL,
  `description` tinytext NOT NULL,
  `observations_count` smallint(6) DEFAULT NULL,
  `taxa_count` smallint(6) DEFAULT NULL,
  `menu_flag` tinyint(3) unsigned DEFAULT '0',
  `smart_flag` tinyint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.projects: ~4 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `title`, `slug`, `icon_url`, `description`, `observations_count`, `taxa_count`, `menu_flag`, `smart_flag`) VALUES
	(3280, 'Golan', 'deers', '', 'Observation of boars in Israel', 0, 0, 0, 0),
	(4527, 'Tatzpiteva - תצפיטבע', 'tatzpiteva', 'http://www.inaturalist.org/attachments/projects/icons/4527/span2/appIcon_120x120.png?1445342416', 'Welcome to TAZPITEVA,  Golan ecological community monitoring project.\r\n\r\n We invite you to participate in recording and uploading observations of animals, plants and water sources in the Golan heights. The aim of this project is to  learn and better under', 354, 101, 1, 2),
	(5759, 'Golan RoadKill - בעלי חיים דרוסים בגולן', 'golan-roadkill', '', 'בעלי חיים דרוסים  בכבישי הגולן הינה תופעה קשה אך משקפת נוכחות בע"ח באזורי המחיה השונים  ומאפשרת איתור  אזורים מועדים בהם מתקיים ריבוי של דריס', 10, 10, 1, 1),
	(6275, 'מדרשת הגולן', '980929b2-755e-4ad0-8eb9-80741e8bf317', '', '', 8, 0, 0, 0);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;


-- Dumping structure for table GolanLayer.texts
CREATE TABLE IF NOT EXISTS `texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) DEFAULT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table GolanLayer.texts: ~6 rows (approximately)
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;
INSERT INTO `texts` (`id`, `slug`, `content`) VALUES
	(1, 'about', '<p>&#34;תצפיטבע&#34; - מיזם קהילתי, המשותף למועצה האזורית גולן, המכון לחקר הגולן ואוניברסיטת חיפה, ועיקרו, שיתוף הקהילה בניטור רציף של הטבע הגולני.<br/><br/>מתוך ההכרה <b>בחשיבות תיפקודן של המערכות האקולוגיות בגולן</b>, וכהמשך ישיר לתוכנית האב לשטחים הפתוחים בגולן עלה הצורך לעקוב ולנטר את מצב הטבע באזור.היישומון תצפיטבע פותח על בסיס הידע שנצבר במקומות שונים בעולם, . ומאפשר לכל אחד מתושבי הגולן, ובהמשך, לכל מטייל באשר הוא, להשתתף באופן פעיל במעקב רציף אחר הטבע הגולני. בעזרת תצפיטבע נוכל לעקוב אחר צמחים, בעלי חיים ומקורות מים, תוך שותפות בין אנשי מקצוע לבין קהילת הגולן. תיעוד הטבע הגולני ע&#34;י תושבי הגולן ישמש את החוקרים להבנה טובה יותר של השינויים החלים במערכות <a href="http://google.com" target="">האקולוגיות</a>, ויהווה בסיס ידע לקבלת החלטות של קובעי המדיניות באזור.<br/><br/>המשתמשים ביישומון יוכלו, בכל זמן נתון, לתעד צמחים ובעלי חיים ולהעבירם אל מאגר הנתונים של צוות המיזם. במקביל לתהליך זה יתקיים היזון חוזר ומשוב אל המנטרים, (קהילת הגולן) ויועבר מידע רלבנטי העוסק בנושאי טבע וסביבה באופן כללי.<br/><br/>המידע והעדכון השוטף באתר תצפיטבע נועד <u>לשימושם</u> של תלמידים, סטודנטים, <i>חוקרים, וכל מי שיש לו עניין בשימור הטבע הגולני.</i><br/><br/>המיזם &#34;תצפיטבע&#34;, ישמש בעתיד את כלל תושבי ארצנו, בכל אזורי הנופש והטיולים הפרושים לרוחבה ולאורכה של ארצנו. תיעוד הטבע ע&#34;י הקהילה , יהדק את הקשר שבין התושבים לבין עצמם ובין התושבים לטבע שסביבם, תוך שמירה על המערכות האקולוגיות והסביבה שבה אנחנו חיים, ויבטיח איכות חיים לכולנו.</p>'),
	(2, 'activity', '<p>שלום וברוכים הבאים לתצפיטבע.<br/>אנו נמצאים בעיצומו של שלב בטא בפיתוח היישומון (אפליקציה) והאתר הנלווה.<br/>קבוצות מתנדבים מקרב תושבי הגולן, נוטלים חלק בהתנסות ראשונית, שמאפשרת למפתחים לשדרג את יכולות האתר והיישומון.<br/>במקביל מתקיימת עבודה רבה ליצירת קשר בלתי אמצעי בתחום הקהילתי – שיתוף תושבי הגולן, פעילות במערכות החינוך הפורמלי והבלתי פורמלי.<br/>חוקרים מתחומי האקולוגיה, בוטניקה וזואלוגיה, בוחנים אזורים לניטור ומושאי ניטור, שיענו על שאלות מחקריות העולות כרגע בהקשר לשמירת הטבע הגולני.<br/><br/></p>'),
	(3, 'contact', '<p>ניתן ליצור איתנו קשר בטופס מטה. נשמח לשמוע מכם בכל עניין.</p>'),
	(4, 'links', 'להלן רשימה קצרה של קישורים \\ מאמרים שכדאי לקרוא.'),
	(5, 'about_bottom', '<p>קחו חלק בקהילה המתרחבת של מתנדבים המנטרים את הטבע הגולני.<br/></p>'),
	(6, 'join', '<p><br/>מטרת האפליקציה והאתר היא לאפשר לכל אדם באשר הוא -בטיול, פיקניק או כל פעילות אחרת בטבע, לצלם ,לתעד ולהעלות תצפיות של בעלי חיים ,צמחים ובתי גידול לחים בגולן.<br/>ניתן להעלות תצפית ישירות בשטח - דרך הפלאפון ( אנדרואיד ואיפון ).<br/><br/>בנוסף האתר והאפליקציה מאפשרים להעלות את התצפית, גם לאחר היציאה מהשטח, דרך המכשיר הסלולרי או אתר האינטרנט.<br/>בעזרת היישומון תוכלו לזהות את מין החיה או הצמח, לעזור לאחרים לזהות תצפיות שלהם, לכתוב הערות על תצפיות מעניינות, ללמוד על החי וצומח של הגולן ובכך לתרום לפרויקט ייחודי מסוגו של ניטור הטבע הגולני.<br/>צילום בהיר יאפשר זיהוי קל יותר, של בעל החיים או הצמח. מומלץ במקרים של צמחים וחרקים לצלם כמה תמונות מכמה זוויות ( בכל תצפית ניתן להעלות מספר צילומים, מומלץ עד 3-4 ).<br/><br/>בזמן העלאת תצפית קיימת אפשרות להסתיר את המיקום המדויק על מנת לא לפגוע בערכי טבע או מקומות רגישים. כמו כן קיימת אפשרות להעלות תצפיות של חיות דרוסות.<br/><br/>האפליקציה ידידותית וקלה לשימוש:<br/><br/>פתיחה, עמוד ראשי. לחיצה על אייקון המצלמה. צילום (או מספר צילומים).<br/><br/>מעבר לדף תצפית. השלמת פרטים: זיהוי, הערות, מוסתר ,חי או מת וכו...<br/><br/>העלאת תצפית למערכת. (אין צורך בהתנתקות כל פעם ניתן להישאר מחוברים וזמינים לתצפית הבאה.)<br/><br/>בכל כניסה יש למשוך את המסך כלפי מטה לסנכרון תצפיות קודמות.<br/><br/>&#34;במצב מפה&#34; באפליקציה ניתן לראות תצפיות שלי, של אחרים, לבצע זיהוי ולהתעדכן על מה שקורה בסביבה.<br/><br/>אתר תצפיטבע מאפשר בנוסף להעלאת תצפיות, חיבור אל תצפיות של משתמשים אחרים, אפשרות להשתתף בזיהוי ודיון על התצפיות, חשיפה למאמרים מקצועיים, חיפושים מתקדמים של מינים , חיפוש לפי משתמשים, הודעות מצוות הפרויקט , קישורים למגדירים ומקורות מידע אחרים ועוד...<br/><br/>בכל בעיה או שאלה ניתן לפנות לצוות הפרויקט .<br/></p>');
/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
