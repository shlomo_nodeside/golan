/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('golan')
  
    .constant('config', {
      //test        : true,
      test        : false,
      env			: 'dev',
      //env		: 'prod',
      appName		: 'Golan',
      appVersion	: 0.1,
      apiUrl		: 'http://www.inaturalist.org/'
    });

})();
