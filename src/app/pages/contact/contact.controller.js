(function() {
  'use strict';
  
  angular.module('golan')
    .controller('ContactController', ctrl)
  ;
  
  function ctrl($scope, restApi) {
      
      $scope.msg = {};
      $scope.loading = false;
      $scope.formDisabled = false;
      $scope.formSuccess = false;
      
      
      //  Get intro Text
      $scope.text = '';
      restApi.cake.texts('contact').then(function (text) {
        $scope.text = text;
      });
      
      //  Send Function
      $scope.send = function (form) {
        
        $scope.loading = true;
        
        //  Manualy make sure all fileds are checked
        angular.forEach(form.$error.required, function (field) {
          field.$setDirty();
        });
        
        //  All Ok, Send the fucker
        if (form.$valid) {
          
          $scope.formDisabled = true;
          restApi.cake.sendContact($scope.msg).then(
            function (data) {
              $scope.loading = false;
              if (data) {
                $scope.formSuccess = true;
              } else {
                $scope.formDisabled = false;
              }
            },
            function () {
              alert('שגיאה בשליחה הודעה');
            }
          );
          
        } else {
          $scope.loading = false;
        }
      };
    }
  
})();