<?php

$intArr = array(
	'urls'  => array(
		'list'  => $this->Html->Url('/json/links/index/true', true),
		'del'   => $this->Html->Url('del', true),
		'add'   => $this->Html->Url('add', true)
	)
);

$this->Js->set('golanBackEnd', $intArr);
echo $this->Js->writeBuffer(array('onDomReady' => false));

?>
<div class="links" ng-controller="linksCtrl">
	<div class="page-header">
		<h3>רשימת קישורים</h3>
	</div>

	<div class="row">

		<div class="col-md-6">

			<div class="panel panel-primary cc-list">
				<div class="panel-heading">רשימת קישורים</div>

				<div class="panel-body">

					<div class="list-group">
						<div class="list-group-item" ng-repeat="item in items" ng-class="{active: item.id === activeItem.id}">
							<h4 class="list-group-item-heading">
								{{item.title}}
							</h4>
							<div class="pull-left actions">
								<i class="fa fa-times pointer" title="מחק קישור" ng-click="del(item)"></i>
								<i class="fa fa-pencil pointer" title="ערוך קישור" ng-click="edit(item)"></i>

							</div>
							<p class="list-group-item-text">{{item.url}}</p>
						</div>
					</div>

					<div class="alert alert-warning" ng-show="!items.length">לא נמצאו קישורים</div>
				</div>
			</div>

		</div><!-- col-6 -->

		<div class="col-md-6">
			<cc-add-edit-link item="activeItem"></cc-add-edit-link>
		</div>

	</div><!-- row -->
</div>


<script type="text/ng-template" id="confirmDelModal.html">
	<div class="modal-header">
		<h5 class="modal-title">
			<span>האם למחוק את</span>
			'{{item.title}}'?
		</h5>
	</div>
	<div class="modal-footer tal">
		<button class="btn btn-default" type="button" ng-click="cancel()">ביטול</button>
		<button class="btn btn-primary" type="button" ng-click="ok()">מחק</button>
	</div>
</script>