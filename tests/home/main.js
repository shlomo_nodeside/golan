// Customers - index

describe('TAZ Home', function() {

	// afterAll( function(){
	// 	browser.sleep(2000);
	// });
	// default sleep time
	var snooze = 100;
	if( typeof browser.params.sleep != 'undefined' )
		snooze = browser.params.sleep;

	it('should load main elements on page', function(){
		browser.get('');

		// site logo
		expect($('.navbar-header a.navbar-brand img').isPresent()).toBe(true);
		// sponsor logo
		expect($('a.navbar-brand.regional img').isPresent()).toBe(true);
		// free search
		expect($('.crumb .row .free-search').isPresent()).toBe(true);
		// project selector
		expect($('.crumb .row .projects').isPresent()).toBe(true);
		// animal filters
		expect($('.crumb .row .filters').isPresent()).toBe(true);
		// user actions buttons
		expect($('.crumb .row .user-actions').isPresent()).toBe(true);
		// right list of items
		expect($('#home-map .row .items').isPresent()).toBe(true);
		// left, map display
		expect($('#home-map .row .map').isPresent()).toBe(true);
	});

	it('should have a navigation bar on top', function(){
		
		// element.all(by.css('.navbar-header ul.navbar-nav li'))
	});

/*
	it('should have a help button', function(){
		// have help button
		expect($('#help_button').isPresent()).toBe(true);
		// target is blank
		$('#help_button').getAttribute('target').then( function(val) {
			expect(val).toEqual('_blank');
		});
		// links is correct
		$('#help_button').getAttribute('href').then( function(val) {
			expect(val).toEqual('https://docs.campaigngo.com/display/docs/Buyers+Purchases');
		});
	});

	it('should load the first customer view', function(){
		// click 2nd table row
		element.all(by.css('table.customers-list tr')).get(1).click();
		browser.sleep(snooze);

		expect($('.customer-view .dd-list').isPresent()).toBe(true);
	});

	it('should have a customer summary table', function(){
		expect($('.customer-view table.summery').isPresent()).toBe(true);
	});

	it('should allow to view all customers', function(){
		// click the filter button
		element.all(by.css('#customers-widget .widget-buttons a')).first().click();
		browser.sleep(snooze);
		expect($('#customers-widget').isPresent()).toBe(true);
	});

	it('should allow to view only repeat customers', function(){
		// click the 2nd filter button
		element.all(by.css('#customers-widget .widget-buttons a')).get(1).click();
		browser.sleep(snooze);
		expect($('#customers-widget').isPresent()).toBe(true);
	});
	*/
});