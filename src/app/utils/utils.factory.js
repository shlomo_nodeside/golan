(function() {
  'use strict';

  angular
    .module('golan')
    .factory( 'ccUtils', ccUtils)
    .service( 'ccSmartNotify', ccSmartNotify)
  ;

  function ccUtils(Globals, $window) {

    var globals   = Globals.get(),
        imgSizes = {
          square: 'square_url',
          medium: 'medium_url',
          large:  'large_url'
        },
        windowPreventLeave = {
          text : null,
          event : function (event) {
            event.returnValue = windowPreventLeave.text;
          }
        }
      ;


    var utils = {

      getItemImage: function(item, size){

        size = size || 'square';

        //  If item has own image
        if(angular.isDefined(item.photos[0])){

          return item.photos[0][imgSizes[size]];
        }
        //  No? Look For item identification image
        else if(item.identifications && angular.isDefined(item.identifications[0])){

          if(size === 'square'){
            return item.identifications[0].taxon.image_url;
          }

        }

        //  Still NADA? return place holder
        //return 'assets/images/item-holder-'+ imgSizes[size] +'.png';
        return null;
      },

      getItemTitle: function(item){
        //  If item has taxon
        //console.log(item);
        if(item.taxon){

          //  Has Common Name?
          if(item.taxon.common_name){
            return item.taxon.common_name.name;
          }else {
            return item.taxon.name;
          }

        }else{  //  No Taxon return segested name
          return item.species_guess;
        }
      },

      //  Check if user is curator (by user id)
      isUserCurator: function(userSlug) {
          if(!globals.curators){
            globals = Globals.get();
          }

          return globals.curators.indexOf(userSlug) !== -1;
        },

      /**
       * Prevent user from leaving this page
       * target (bol)- Set this on/off
       */
      preventWindowLeave: function(target, text){

        function add(){
          if ($window.addEventListener) {
            $window.addEventListener("beforeunload", windowPreventLeave.event);
          } else {
            //For IE browsers
            $window.attachEvent("onbeforeunload", windowPreventLeave.event);
          }
        }

        function remove() {
          if ($window.removeEventListener) {
            $window.removeEventListener("beforeunload", windowPreventLeave.event);
          } else {
            $window.detachEvent("onbeforeunload", windowPreventLeave.event);
          }
        }

        if(target){
          windowPreventLeave.text = text;
          add();
        }else {
          remove();
        }
      }

    };

    return utils;
  }

  function ccSmartNotify(Notification, $q, $rootScope) {

    return {
      notify : function(promisess) {



        var current = 0,
            total = promisess.length,
            notify,
            localScope = $rootScope.$new(true)  //  Create a new scope for the notify element
          ; 

        //  Set Start values to notify scope
        localScope.current = current;
        localScope.total = total;
        localScope.class = 'primary';

        //  Create the notify object
        notify = Notification.primary({
          delay   : null,
          templateUrl: 'app/components/directives/partials/loader-notify.html',
          scope: localScope
        });
        
        //  Hook action to each of the promises
        angular.forEach(promisess, function(promise){
          promise.then(
            function(){ //  Promise Returns success
              current++;
              if(current <= total){
                localScope.current = current;
              }
            },function (e) {  //  Promise Returns error
              current++;
              //  One Promise return error
              localScope.class = 'error';
              localScope.error = true;
            });
        });

        //  When all promisess resolved
        $q.allSettled(promisess).then( function(){

          //  Close The loader Notify
          notify.$$state.value.kill();

          if(localScope.error){
            Notification.error('חלק מהתמונות נכשלו, נסו שוב.');
          }else {
            Notification.success('כל התמונות עלו בהצלחה');
          }

        });
      }
    }
  }

})();
