(function() {
  'use strict';

  angular
    .module('golan')
    .config(config)
    .factory( 'Globals', globalsProv)
    .factory('ccHttpRequestInterceptor', ccHttpRequestInterceptor)
  ;

  /** @ngInject */
  function config($logProvider, uiGmapGoogleMapApiProvider, $httpProvider, NotificationProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    
    //  Google Maps 
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyDNkjs2j9ZhdyE8OBta5OhjL8-k4Em-_KI',
      //v: '3.20',
      v: '3.22',
      //libraries: 'weather,geometry,visualization'
      language: 'he'
    });
  
    $httpProvider.interceptors.push('ccHttpRequestInterceptor');
  
    NotificationProvider.setOptions({
      delay: 5000,
      startTop: 185,
      startRight: 0,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'left',
      positionY: 'top'
    });
  }
    
  function globalsProv($rootScope) {
    
    var globals = {
      data: {},
      set : function(data){
        this.data = data;
        $rootScope.$broadcast('globals');
      },
      get : function(){
        return this.data;
      }
    };
    
    return globals;
    
  }

  /**
   * Small Service that adds Auth header if requested in $http config
   * use: tokenRequired: true
   * @param $q
   * @param Globals
   * @returns {{request: request}}
   */
  function ccHttpRequestInterceptor($q, Globals) {

    return {
      request: function (config) {

        //console.log(config);
        if(!config.tokenRequired) {
          return config;
        }

        var token = Globals.get().aToken;

        //  Token Is required but is not set, Cancel Request
        if(!token) {
          var canceler = $q.defer();
          config.timeout = canceler.promise;
          canceler.resolve({statusText: 'No Auth Set'});
        }

        //  Has Token, And Needed - Add To headers
        config.headers.Authorization = 'Bearer '+token;

        return config;
      }
    };
  }

  // Modifies $httpProvider for correct server communication (POST variable format)
  angular.module('httpPostFix', [], function($httpProvider)
  {
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data)
    {
      /**
       * The workhorse; converts an object to x-www-form-urlencoded serialization.
       * @param {Object} obj
       * @return {String}
       */
      var param = function(obj)
      {
        var query = '';
        var name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj)
        {
          value = obj[name];

          if(value instanceof Array)
          {
            for(i=0; i<value.length; ++i)
            {
              subValue = value[i];
              fullSubName = name + '[' + i + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value instanceof Object)
          {
            for(subName in value)
            {
              subValue = value[subName];
              fullSubName = name + '[' + subName + ']';
              innerObj = {};
              innerObj[fullSubName] = subValue;
              query += param(innerObj) + '&';
            }
          }
          else if(value !== undefined && value !== null)
          {
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
          }
        }

        return query.length ? query.substr(0, query.length - 1) : query;
      };

      return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];
  });
})();


